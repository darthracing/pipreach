#!/usr/bin/perl
#
# long2clip - request clip of defined duration from a long wav on the local archive
#
# T.Stiers 2019-0217
#
# darthracing.com 2019

use strict;
use CGI;

my $q = CGI->new;
my %in = $q->Vars;

my $SERMONPUSH="/mnt/usb3/sermonpush/";

my $MYCGI="/cgi-bin/long2clip.cgi";

my $date=localtime();

my $wavpath=&getarg("wavpath","",%in);
my $clipstart=&getarg("clipstart","",%in);
my $clipend=&getarg("clipend","",%in);

print<<HEAD;
Content-type: text/html

<html>
<head>
<title>long2clip: $date</title>
</head>
<body>
<form method="POST" action="${MYCGI}">
long2clip: $date: $in{test}
<hr>
HEAD

if ( $wavpath ne "" && $clipstart ne "MM:SS" && $clipend ne "MM:SS" ) {
	my ($result,$msg)=&makeclipjob($wavpath,$clipstart,$clipend);
	# print "result=$result, msg=$msg\n";
	if ( $result == 0 ) {
		$wavpath=""; # clear, new form
	}
} 
	
if ( $wavpath eq "" ) {
	print "<table><tr><td>full wav:<select name=\"wavpath\">\n";
	my @ar=&longlist("2019");
	foreach my $i (@ar) {
		print "<option>$i\n";
	}
	print "</select></td>\n";
	print "<td>start:<input type=\"text\" name=\"clipstart\" size=5 value=\"MM:SS\"></td>\n";
	print "<td>end:<input type=\"text\" name=\"clipend\" size=5 value=\"MM:SS\"></td>\n";
	print "<td><input type=\"submit\" name=\"thesubmit\" value=\"generate clip\"></td></tr>\n";
	print "</table>\n";	

}

print<<TAIL;
</form>
<hr>darthracing.com cw 2019</body></html>
TAIL

#
# functions
#
sub getarg {
	my($key,$df,%h)=@_;
	if ( defined ($h{$key}) ) {
		return $h{$key};
	} else {
		return $df
	}
}

sub longlist {
	my($year)=@_;
	my $YEAR="????";
	if ( $year != "" ) {
		$YEAR=$year;	
	}
	my $cmd="cd $SERMONPUSH; ls -t -1 ${YEAR}-????/????-????_??-??.wav";
	my @arr;
	open(CMD,"$cmd |");
	while(my $line=<CMD>) {
		chop($line);
		push @arr,$line;
	}
	close(CMD);
	return @arr;
}

sub makeclipjob {
	my($wav,$cstart,$cend)=@_;
	my($dir,$file)=split /\//,$wav;
	my($filedate,$wavstart,$wavsuffix)= split /_|\./,$file;

	# check syntax
	if ( $cstart !~ /[0-9][0-9]:[0-5][0-9]/ or $cend !~ /[0-9][0-9]:[0-5][0-9]/ or $dir !~ /[0-2][0-9]-[0-5][0-9]/ ) {
		my $badsyntax="badsyntax: dir=$dir [HH-MM], clipstart=$cstart [MM:SS], clipend=$cend [MM:SS]";
		print "<pre>$badsyntax</pre>\n";
		return (0,$badsyntax);
	}

	my $pid=$$;
	# my $jobfile="/tmp/long2clip.job_$pid";
	my $jobfile="/tmp/long2clip/long2clip.job";
	my $bashjob=<<MAKEJOB;
#!/bin/bash
#
# $jobfile
#
# dir=$dir 
# wavstart=$wavstart
# cstart=$cstart
# cend=$cend
#
PATH=/bin:/usr/bin

cd $SERMONPUSH

./long2podcast.sh $wavstart $cstart $cend $dir
./push-to-db.sh $dir

MAKEJOB

	print "<pre>$bashjob</pre>\n";

	open(JFILE,">$jobfile");
	print JFILE $bashjob;
	close(JFILE);
	system("chmod a+rwx $jobfile");
	return (1,$bashjob);
}
