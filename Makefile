.PHONY: all clean install

all: deb

clean:
	rm -rf debian/pipreach
	rm ../pipreach*

deb:
	dpkg-buildpackage -b
