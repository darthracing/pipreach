#!/bin/sh

PATH=/bin:/usr/bin

while true; do
        logger "vpn_linode_http: making connection..."
        ssh -o ConnectTimeout=15 -g -R 2080:localhost:80 tas@linode "sleep 7200" > /tmp/vpn_linode_http.log 2>&1
        logger "vpn_linode_http: lost connection..."
        sleep 15

done
