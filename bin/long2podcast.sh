#!/bin/bash
#
# long2podcast.sh - put in clip boundaries and push to sites
#
# T.Stiers 2018-1028 - update to specific filename vs service 1v2
# T.Stiers 2019-0218 - add more to "short" use time ranges
#
PATH=/bin:/usr/bin:/usr/local/bin

cd /home/pi/sermonpush

DATE=`date +%Y-%m%d`

if [ "$3" = "" ]; then
    echo "usage: $0 <HH-MM> <starttime MM:SS> <endtime MM::SS> [YYYY-MMDD]"
    exit 1
fi

if [ "$4" ]; then
    DATE=$4
fi

TIME=$1
STARTTIME=$2
ENDTIME=$3

if [ ! -d $DATE ]; then
    echo "no directory $DATE found, exiting!"
    exit 1
fi

cd $DATE

DATE="${DATE}_${TIME}"

WAV=${DATE}.wav
if [ ! -s ${WAV} ]; then
    echo "unable to find file $WAV...exit..."
    exit 2
fi

# start end
# - sox long.wav short.wav trim 11:27 =20:27
# - compand to make it sound better
# -- podcast formula: https://forum.doom9.org/showthread.php?t=165807
TIMELABEL=`echo "${STARTTIME}-${ENDTIME}" | sed 's/://g'`
WAVSHORT=${DATE}_${TIMELABEL}_short.wav
sox $WAV -c 1 ${WAVSHORT} trim $STARTTIME =${ENDTIME} compand 0.3,1 6:-70,-60,-20 -5 -90

# make an mp3
MP3SHORT=${DATE}_${TIMELABEL}_short.mp3
lame ${WAVSHORT} ${MP3SHORT}

# make mp3 of full service
# lame $WAV -a ${WAV}.mp3

# push new file to dropbox
/home/pi/sermonpush/push-to-db.sh $DATE
