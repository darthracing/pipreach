#!/bin/bash
#
# T.Stiers 2018-0228
# 
PATH=/bin:/usr/bin:/usr/sbin:/sbin

DUR=4800
if [ "$1" ]; then
	DUR=$1
fi

DATE=`date +%Y-%m%d_%H-%M`
YMON=${DATE%%_*}

DIR="/home/pi/sermonpush/${YMON}"

if [ ! -d $DIR ]; then
	mkdir -p $DIR
fi

OFILE="${DIR}/${DATE}.wav"
STATFILE="${DIR}/${DATE}_status.html"
LOGFILE="${DIR}/${DATE}_record.log"

# functions
logit () {
	MSG=$*
	LINE=`date`"[$$] $MSG"
	echo $LINE
	echo $LINE >> $LOGFILE
	echo "<html><body>$LINE</body></html>" > $STATFILE
}

# main
logit "record process started: ${OFILE}"
TRY=0
MAXTRY=3
RECORDING=0
while [ $RECORDING -eq 0 -a $TRY -le $MAXTRY ]; do

	# duration broken: arecord --device=hw:1,0 --format S16_LE --rate 44100 --duration=$DUR -c1 ${OFILE}
	# (arecord --device=hw:1,0 --format S16_LE --rate 44100 --duration=$DUR -c1 ${OFILE}) &
	(arecord --device=hw:1,0 --format S16_LE --rate 44100 --duration=$DUR -c1 - > ${OFILE} ) &
	APID=$!
	# test that APID exists after 5 seconds
	logit "audio record process: APID=$APID"
	sleep 5
	kill -0 $APID
	if [ "$?" -ne 0 ]; then
		((TRY=TRY+1))
		logit "no process $APID, TRY=$TRY"
	else
		RECORDING=1
	fi
done

if [ $RECORDING -eq 0 ]; then
	logit "still not recording, exit..."
	exit 2
fi

logit "okay, recording for $DUR"
sleep $DUR
kill $APID

# settle
sleep 2

#
OFILESTAT=`ls -l $OFILE >2&1`
if [ -s "$OFILE" ]; then
	logit "file exists: OK: $OFILESTAT"
else
	logit "no file: FAIL: $OFILESTAT"
	exit 2
fi

OFILEMP3=${OFILE%%.*}.mp3
logit "recording duration complete, generating mp3 $OFILEMP3"
lame ${OFILE} -a ${OFILEMP3}
if [ -s ${OFILEMP3} ]; then
	logit "mp3 conversion completed OK"
else
	logit "mp3 conversion FAIL!"
fi
logit "record process complete"

#
# push attempt
/home/pi/sermonpush/push-to-db.sh
