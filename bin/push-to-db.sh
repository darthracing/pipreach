#!/bin/bash
#
# push-to-db - push sermonpush files to dropbox
#
# T.Stiers 2018-1028
#
PATH=/bin:/usr/bin:/usr/local/bin:~/bin/

DATE=`date +%Y-%m%d`

if [ "$1" ]; then
        DATE=$1
fi

cd ~/sermonpush

MP3FILES=`ls ${DATE}/*.mp3`

for MP3 in `echo ${MP3FILES}`; do
        CMD="dropbox_uploader.sh -s upload ${MP3} ${MP3}"
        echo "CMD: $CMD"
        $CMD
done

