#!/bin/bash
#
# run-long2clip.sh
#
PATH=/bin:/usr/bin

TMP=/tmp/long2clip

if [ ! -d $TMP ]; then
	mkdir -p $TMP
	chmod a+rwx $TMP
fi


cd $TMP
if [ -s long2clip.job ]; then
	mv long2clip.job long2clip.sh
	sh -x long2clip.sh > long2clip.errlog 2>&1
	rm -f long2clip.sh
fi
